/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.List;

/**
 *
 * @author user
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice(){
        ArtistDao artistdao = new ArtistDao();
        return artistdao.getArtistByTotalPrice(10); 
    }
    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin, String end){
        ArtistDao artistdao = new ArtistDao();
        return artistdao.getArtistByTotalPrice(begin, end, 10); 
    }
}
